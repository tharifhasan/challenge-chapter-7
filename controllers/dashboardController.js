const { users } = require("../models");
const passport = require("../lib/passport");

exports.index = (req, res) => {
  const title = "Dashboard User Game";
  const subTitle = "Welcome Admin to dashboard of user game";
  const userData = req.user.dataValues;
  const user_game_history = res.render("web/dashboard", {
    title,
    subTitle,
    userData,
  });
};

exports.about = (req, res) => {
  const title = "About Me";
  const subTitle = "Here is about me";
  res.render("web/about", { title, subTitle });
};

exports.GetUsers = async (req, res) => {
  const UserData = await users.findAll();
  res.render("dashboard/dashboard", {
    title: "Dashboard",
    layout: "layouts/second-layout",
    UserData,
  });
};
