const {rooms} = require("../models")

exports.create = (req,res) => {
    var name = Date.now();          // 1669641824865
    name = String(name);

    rooms.create({
        name:name
    })
    .then(rooms => {
        res.status(200).json(rooms)
    })
    .catch(err => {
        res.status(500).json(err)
    })

    // {
    //     "id": 2,
    //     "name": "1669641947326",
    //     "updatedAt": "2022-11-28T13:25:47.327Z",
    //     "createdAt": "2022-11-28T13:25:47.327Z"
    // }
}