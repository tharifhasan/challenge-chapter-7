var express = require("express");
var router = express.Router();

const index = require("../controllers/indexController");

router.get("/", (req, res) => {
  res.render("index", {
    title: "Main Page",
    layout: "layouts/main-layout",
  });
});

router.get("/game", (req, res) => {
  res.render("gameplay", {
    title: "Welcome to paper rock scissor",
    layout: "layouts/game-layout",
  });
});

module.exports = router;
