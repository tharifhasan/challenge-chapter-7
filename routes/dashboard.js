const express = require("express");
const router = express.Router();
const { GetUsers } = require("../controllers/dashboardController.js");
// const restrict = require("../middlewares/restrict");

router.get("/dashboard", GetUsers);
module.exports = router;
